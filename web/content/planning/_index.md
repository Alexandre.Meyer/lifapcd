---
title: "Planning"
description: "Planning"
---

## Printemps 2025

<img src="doc/2025_edt.png" width="800" class="center">

<a href="http://adelb.univ-lyon1.fr/direct/index.jsp?projectId=3&ShowPianoWeeks=true&showTree=false&resources=95569&displayConfName=_DirectPlanning_DOUA_CHBIO&days=1&weeks=22,23,24,25,26,27,29,30,31,32,33,34,35" target = "_blank" >Ouvrir sur ADE</a><br/>


Les TD

* Quai 43 s.102 (1er étage) : Nicolas PRONOST
* Quai 43 s.105 (1er étage) : Franck FAVETTA
* Quai 43 s.106 (1er étage) : Erwan GUILLOU
* Quai 43 s.109 (1er étage) : Quentin DESCHAMPS
* Berthollet 203 (2e étage) : Gabriel MEYNET
* Berthollet 204 (2e étage) : Alexandre MEYER

Les TP/rapporteurs de projet : intervenants de TD + Elodie DESSEREE, Nicolas LOUVET

Salles pour les projets : Quai 43, 102, 105, 106, 109, 111, 112, 113


## Les dates importantes

* jeudi 20 février 18h : module Image à rendre dans TOMUSS (10% de la note finale)
* mardi 25 février 8h-9h30 : examen en amphi  (40% de la note finale)
* lundi 10 mars à 18h : cahier des charges à rendre dans TOMUSS (2% de la note finale)
* mardi 18 mars : démo mi-parcours (3% de la note finale)
* mardi 22 avril : soutenance de projet (3 notes : Technique 15%, Conception 15%, Organisation 15%)
