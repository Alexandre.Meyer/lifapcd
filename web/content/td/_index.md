---

title: "Travaux Dirigés"
description: "TD"
---


<br>
Il y a des TD au tableau/stylo et des TD sur machine. Pour la partie marchine, le module Image est à rendre.
<br>


### TD-C Conception et diagramme des classes

<img src="doc/conception.jpg" width="100" class="center">

Etude de la conception de différentes applications simples, diagramme des classes.
* [TD-C1 : Parc de voitures, Pacman](doc/LIFAPCD_TD-C1_ParcvoiturePacman.pdf)
  * [Elements de correction du TD Conception 1](doc/LIFAPCD_TD-C1_ParcvoiturePacman_corriges.pdf)
* [TD-C2 : application d'assemblage de Duplo et introduction aux TD outils](doc/LIFAPCD_TD-C2_Duplo.pdf)
  * [Elements de correction du TD Conception 2](doc/LIFAPCD_TD-C2_Duplo_corrige.pdf)

<br>
<br>


### TD-O Outils

<img src="doc/tools.png" width="200" class="center">

#### TD-O0 Installation et choix d'un éditeur de code

Choisisser un éditeur de code. Nous vous conseillons de choisir parmi VSCode, Visual Studio(Windows), XCode(MacOS), CLion ou Codeblocks (Windows/Linux). [Regarder les consignes d'installation ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/install.md)

<br>


#### TD-O1 Classe Image

Voir l'énoncé de la [Partie 1](doc/LIFAPCD_TD-O1_ClasseImage.pdf)

* Ecriture de la structure Pixel et de la classe Image
* Organisation des fichiers
* Début des tests de la mémoire avec Valgrind  

<br>


#### TD-O2 Gestionnaire de version et documentation

* Passage du code dans un projet git
  * Se connecter sur la [forge Lyon 1](https://forge.univ-lyon1.fr)
  * Dupliquez [le projet suivant](https://forge.univ-lyon1.fr/Alexandre.Meyer/l2_cda_moduleimage). Il comporte l'éxécution du script de notation dans sa version tout automatique. Attention : puor la notation votre archive sera testée avec un humain qui regarde votre code.
  * Ajoutez-y les membres de votre groupe
  * [Réalisez le tutorial "Premier pas" se trouvant ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/git.md)
* Documentez la classe Image et la structure Pixel avec les balises doxygen et générez une documentation HTML. [Voir les explications générales sur doxygen ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/doxygen.md).  
  * Pour votre module Image
    ```
    ~/LIFAPCD/modIm $ mkdir doc
    ~/LIFAPCD/modIm $ doxygen -g doc/doxyfile
    ~/LIFAPCD/modIm $ code doc/doxyfile
    // éditer les balises du fichier doxyfile
    ~/LIFAPCD/modIm $ code Makefile
    ```
    Ajouter une règle dans votre Makefile
    ```
    docu:
      doxygen doc/doxyfile

    ```
	Faîtes en sorte que les fichiers générés se trouvent dans `doc`et non à la racine. 
	Il ne doit y avoir que le répertoire `html` de généré, sinon c'est votre fichier doxyfile qui est à modifier.
	De plus, modifiez la règle `clean` pour effacer les fichiers générés.
	```
	clean:
		rm -rf obj/* doc/html
	```
<br>


#### TD-O3 Débogage

Voir l'énoncé de la [Partie 3](doc/LIFAPCD_TD-O3_Debogage.pdf)

* Gestion de mémoire et optimisation de code sur un exemple "jouet"
* Débogage des fonctions de sauvegarde et de chargement d'une image
* Gdb
* Valgrind

<br>


#### TD-O4 Bibliothèques (lib)

Voir l'énoncé de la [Partie 4](doc/LIFAPCD_TD-O4_SDL.pdf)

* Installer les librairies SDL2
* Écrire la classe ImageViewer
* Faire le diagramme des classes du module Image

<br>
<br>


## Module Image à rendre

<img src="../projet/doc/livrable.png" width="150" class="center">

### À rendre

Votre archive sera testée avec un script particulier (cf. plus bas). Vous devez donc **respecter exactement le format attendu par ce script sinon vous aurez la note zéro**.

**Déposez votre archive sur TOMUSS**, dans la case `DepotModuleImage` du cours. Vous devez **respecter l'heure limite,** car le dépôt sera désactivé après l'heure limite. Une soumission en retard (par email ou autre) entraînera la note zéro pour tout le groupe. Un seul dépôt par groupe d'étudiants (sur n'importe lequel des comptes).

Votre module Image doit respecter les conventions suivantes.

* À faire en groupe de 2 ou 3 étudiants
* Votre archive s'appelle `NUMEROETU1_NUMEROETU2_NUMEROETU3.tgz` ou `NUMEROETU1_NUMEROETU2_NUMEROETU3.tar.gz` (pas de zip) où `NUMEROETU1` est le numéro étudiant du premier membre du groupe, etc.
   * Pour faire le tgz, il faut vous placer dans le répertoire parent du projet, puis entrer `tar cfvz NUMEROETU1_NUMEROETU2_NUMEROETU3.tgz NUMEROETU1_NUMEROETU2_NUMEROETU3`
* Tous les fichiers se décompressent dans un répertoire ayant pour nom `NUMEROETU1_NUMEROETU2_NUMEROETU3` (même nom que l'archive)
* Ce répertoire doit contenir les sous-répertoires `bin`, `src`, `obj`, `data` et `doc`
* Un fichier `Makefile` (donné explicitement ou bien générable par le fichier `CMakeLists.txt` de CMake) compile les trois programmes :
  * `bin/exemple` exécute le code générant les images en sortie (i.e. exécute mainExemple.cpp)
  * `bin/test` appelle `testRegression`  de la classe Image (test de non-régression qui vérifie tout) et affiche les tests effectués à l'écran (i.e. exécute mainTest.cpp)
  * `bin/affichage` fait l'affichage de l'image dans une fenêtre SDL avec zoom/dézoom (i.e. exécute mainAffichage.cpp)
* `readme.md` est un fichier texte expliquant la librairie. Au minimum comment compiler et exécuter, ce que fait le module et chacun des exécutables, l'organisation de l'archive, etc., et il doit indiquer les noms/prénoms/numéros des étudiants et l'id du projet sur la forge
* Vos fichiers sources (.h et .cpp) sont placés dans le dossier `src`
* Les images sauvées et lues seront toujours placées dans le dossier `data`
* Les .o sont générés dans obj (mais non présent dans l'archive)
* Dans le dossier `doc`, vous aurez
  * `doc/doxyfile` est le fichier de configuration de doxygen
  * `doc/html/index.html` est la page d'entrée de la documentation (générée avec doxygen mais non présente dans l'archive)

Ces conventions sont très répandues, et il serait de bonne habitude de les appliquer pour tous vos rendus de TP (info ou autres), ou vos projets futurs publiés sur internet. Vous utiliserez notamment ces conventions pour votre projet.

**Vous n'avez pas le droit de modifier le code fourni.** Les programmes principaux, les noms des fichiers, des classes et structures, des fonctions, des données, etc., doivent être exactement les mêmes que ceux donnés. Ceci conditionne fortement le succès du script.


### Script de test du module Image

Votre archive du module Image sera testée avec le script présent dans le répertoire `TD_moduleImage` [fourni durant le TD ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp), s'appelant evalModuleImage.py (script Python). Avant de soumettre votre archive, vérifiez que l'exécution du script se déroule sans erreur. Pour vous, il y a 3 manières de lancer le script :

* sous linux avec le code du script en tapant la commande `python3 evalModuleImage.py NUMEROETU1_NUMEROETU2_NUMNUMEROETU3.tgz`;
* sur la machine de l'université en [suivant les explications ici](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp);
* en clonant le projet donné en TD-O2, à chaque commit le script est lancé.

#### Script et notation

* Si le script échoue à une étape, le correcteur n'ira pas plus loin (même si la suite est juste).
* Si le script rapporte des erreurs, corrigez les avant de soumettre votre archive.
* Le script vous donne également une note **INDICATIVE** à la fin. **Pour la note finale, des vérifications supplémentaires (non automatiques) seront effectuées par les enseignants.** La note indicative est donc une **note maximale possible**.


#### Précision technique sur ce script

* Ce script doit tourner avec Python 3 (attention à pas utiliser Python 2) et uniquement sous Linux à cause de valgrind. Si vous l'exécutez sur votre machine personnelle, il faut installer python, doxygen, valgrind, SDL2, etc. Voir les installations à faire sur [la page projet](projet).
* Si votre code nécessite l'utilisation du standard CPP11 ou plus, n'oubliez pas d'ajouter l'option `-std=c++11` (ou plus) dans les lignes de commande g++ de votre Makefile.
* Sous MacOS, [regardez les consignes ici](doc/tools_MacOS.pdf) (merci Erwan)
* Sous Windows, installer WSL, puis tout se passe comme sous Linux
