---
title: "Cours"
description: "Cours"
---

![lifapcd.png](doc/supports_de_cours.jpg)



#### Cours 0 : Introduction de l'UE

* [Télécharger les transparents du cours](doc/LIFAPCD_CM0_OrganisationUE.pdf)


#### Cours 1 : Conception et gestion de projet

* [Télécharger les transparents du cours](doc/LIFAPCD_CM1_ConceptionGestionProjet.pdf)
* Méthodes de conception
* Cahier des charges
* Diagramme de Gantt  


#### Cours 2 : Programmation modulaire

* [Télécharger les transparents du cours](doc/LIFAPCD_CM2_ProgrammationModulaire.pdf)
* Diagramme des classes (UML)
* Règle d'intégrité
* Règles de programmation  


#### Cours 3 : Outils pour la programmation

* [Télécharger les transparents du cours](doc/LIFAPCD_CM3_OutilsProgrammation.pdf)
* Compilation de fichier (GCC)
* Compilation de projet (Makefile et CMake)
* Débogage (gdb)  


#### Cours 4 : Gestion du code

* [Télécharger les transparents du cours](doc/LIFAPCD_C4_GestionCode.pdf)
* Règles d'écriture de code en C/C++
* Gestionnaire de code (Git)
* Doxygen
* [Des règles de bonnes pratiques de la programmation](http://perso.univ-lyon1.fr/thierry.excoffier/COURS/COURS/TRANS_COMP_IMAGE/prog.html)  


#### Cours 5 : Notions de programmation C++ "avancée"

* [Télécharger les transparents du cours](doc/LIFAPCD_C5_BibliothequeIntroPOO.pdf)
* Test de régression
* Valgrind : debug mémoire + profiler
* Arguments de main
* Introduction aux operator et aux template en C++
* Introduction à la STL : string, vector, list, etc.
* Notion de POO/héritage pour pouvoir introduire les frameworks gérant une interface (Qt)  

#### Cours 6 : Interface Graphique (Graphical User Interface)

* [Télécharger les transparents du cours](doc/LIFAPCD_C6_GUI.pdf)
* Une interface, qu'est-*ce que cela change ?
* Notion de callback/pointeurs de fonctions
* Principe d'organisation du code (introduction rapide à la notion de MVC)
* Interface en mode texte (un menu)
* Avec SDL2 + 2 mots sur SFML
* Avec "Dear ImGui"
* Avec un framework plus conséquent : Qt  
