---
title: "Projet réalisé en TP"
description: "Projet"
---


<img src="doc/projet.jpg" width="300" class="center">  


### Sujet et critères de notation

Vous êtes libre du sujet de votre projet, mais discutez-en avec les intervenants de TD et TP afin d'obtenir leur accord. [Si vous êtes en panne d'inspiration, voici quelques idées de sujets](./doc/ideas).

* **Le projet est à réaliser en groupe de 2 ou 3 maximum** (4 n'est pas possible)
* [N'oubliez pas de lire les règles à respecter lors de la conception et la réalisation d'un projet](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/coding_rules.md)
* Regardez également les critères de notations (notes "Technique", "Conception" et "Organisation") [dont le détail est ici](https://docs.google.com/spreadsheets/d/1OcpRm6gQtmRNWSXeG7QwnFJ6Eqzl7_DKCjUM4V9P4A0/edit?usp=sharing).  (IMPORTANT A LIRE)
* Vous devez **être autonome ET nous montrer régulièrement l'état d'avancement de votre projet**. Un groupe dont les membres arrivent à 10h voir plus tard, voir pas du tout et qui, tout d'un coup, une semaine avant la soutenance ont un projet bien abouti, sera considéré comme très très suspect.
* Travail en équipe : vous disposez de 100 points par projet. A la fin du projet, vous affectez ces points à chacun des membres du groupe en fonction du degré d'implication. Par exemple, un groupe équilibré avec une personne un peu plus leader donnera 30, 30 et 40 points. Cette information servira à moduler les notes de chacuns (voir les critères de notation dans la grille). La modulation peut aller plus loin que juste les points sur l'organisation du travail. Un étudiant non impliqué peut avoir une note très faible sur les 3 parties, loin du reste du groupe.  


<img src="doc/cahier2.jpg" width="200" class="center">

### Cahier des charges

Après quelques séances de conception et développement, vous devez rédiger et soumettre un cahier des charges (case 'DepotCahierDesCharges' sur Tomuss au format pdf, un seul dépôt par groupe). Ce cahier des charges reprend l'organisation vue en cours et en TD. Il doit comporter au moins une présentation du projet, une description détaillée de l'application (ex. règles du jeu ou fonctionnalités du logiciel), une liste exhaustive et détaillée des tâches à réaliser, un diagramme des classes (UML) et un diagramme de Gantt (tous les deux prévisionels). Ce document fait typiquement entre 4 et 10 pages.  

#### Réaliser votre diagramme des classes (UML)

* [Les bases d'un diagramme de classes sur wikipedia.](https://fr.wikipedia.org/wiki/Diagramme_de_classes) Nous n'utilisons que la notion d'association (et un peu héritage) en LIFAP4 pour les relations entre classes
* Des outils
 *  [Umbrello UML modeller](http://uml.sourceforge.net/) (Linux) est un programme permettant de modéliser les différents diagrammes UML (Unified Language Diagram)
 * [Dia](http://en.wikipedia.org/wiki/Dia_(software)) est un programme
    pour dessiner des diagrammes
 * [StarUML](http://staruml.io/)
 * [www.diagrams.net](https://www.diagrams.net/index.html) : app en ligne ou app à installer (simple, léger et efficace)
 * [Outils online Viual-paradigm](https://online.visual-paradigm.com). Choisissez "Class diagram".
 * [Ce lien](http://en.wikipedia.org/wiki/List_of_Unified_Modeling_Language_tools) répertorie différents outils pouvant être utilisés pou définir des diagrammes de modules

Remarque : normalement sur les machines du Nautibus, Umbrello et Dia sont installés, sinon utilisez un outil en ligne.  



<img src="doc/demo.png" width="200" class="center">

### Démo mi-parcours

A la moitié de votre projet, vous donnerez une démonstration aux intervenants de 10 minutes, qui sera suivie de quelques questions. Le
but de cette démonstration est de faire un point sur ce qui marche et ce qui reste à faire, ainsi que de présenter l'organisation et la gestion de votre projet en général. Vous devez donc à la fois montrer ce que votre application est déjà capable de faire, mais aussi que vous avez les capacités à finaliser le projet à temps.  

<img src="doc/soutenance.jpg" width="200" class="center">

### Soutenance

**La soutenance dure 20 min = 15 min de présentation démo, suivi de 5 min de questions**.
La présentation devra être réalisée sous Powerpoint ou équivalent. Le mieux est de générer un PDF pour des raisons de compatibilité. Voici quelques conseils pour la présentation (entre 8 et 10 minutes).

* Il s'agit d'une présentation technique. Passez donc rapidement sur l'interface graphique et les fonctionnalités, sur lesquelles vous pourrez plus vous attarder durant la démo. La majeure partie devra être consacrée à expliquer comment vous avez conçu et programmé votre application, en décrivant les classes et les structures correspondantes.
* Le fruit d'un travail de plus de 40h doit être décrit en quelques minutes, ce qui est très court. Allez rapidement à l'essentiel ! Le planning étant très serré, vous serez interrompu si vous dépassez le temps imparti.
* Pour les projets réalisés en groupe, le temps de parole devra être équitablement réparti entre les étudiants.
* Le meilleur aperçu de la conception de votre projet reste le diagramme des classes (UML). Soignez sa présentation !

Voici un plan possible.

* Slide 1: Titre du projet, auteurs, principe de l'application (très rapide, avec une capture d'écran)
* Slide 2: **Vue d'ensemble du diagramme des classes (UML)** (donc le diagramme doit être à jour)
* Slide 3 à n-1: Explications détaillées des 3 ou 4 classes les plus importants et/ou les plus intéressants (ex. ceux dont vous êtes les plus fiers, sur lesquels vous avez passé le plus de temps)
* Slide n: Conclusion. Attention à éviter les banalités du style "Le projet a été intéressant..." (ou l'inverse !). Les intervenants vous ont suivis pendant un semestre et ont déjà leur idée là-dessus! Décrivez par exemple ce qui marche et ce qui ne marche pas (les objectifs initiaux du cahier des charges ont-ils été atteints ?), les éventuelles difficultés rencontrées, et ce que vous (re)feriez avec un peu (ou beaucoup) plus de temps.

Quelques conseils pour la démo (entre 5 et 7 minutes).

* Le code doit être compilé, et la démo doit être lancée via l'exécutable
* Préparez la démo pour être lancée tout de suite après la présentation (ex. sur votre ordinateur portable)
* Répétez les actions que vous voulez illustrer, concentrez vous sur ce qui fonctionne (soyez vendeur!)
* Utilisez le plein écran si approprié, prévoyez de quoi entendre sons et musiques si besoin
* Si nécessaire, testez votre démo avant sur écran externe (ie. apprenez comment afficher en double écran sur votre machine)
* **Faîtes au moins une répétition complète pour éviter les surprises (timing, démo qui fonctionne pas, etc.).**  


<img src="doc/livrable.png" width="200" class="center">

#### Travail à rendre (case 'DepotProjetFinal' dans Tomuss)

Préparer et soumettre une archive suivant les mêmes conventions que le module Image nommée `NOM_PROJET_NUMEROETU1_NUMEROETU2_NUMEROETU3.tar.gz` et contenant au minimum les points suivants.

* Un `readme.md` à la racine de l'archive contenant au moins
  * les informations factuelles du projet : noms, prénoms, numéros étudiant et identifiant du projet sur la forge
    * un manuel : commandes de compilation et d'exécution, règles du jeu ou utilisation de l'application, la liste des fonctionnalités du programme
    * une description de l'organisation de votre archive
* Tout le code (dans le répertoire src)
* Un makefile (dans le répertoire racine) ou un fichier CMakeLists.txt
  * Attention, toute autre librairie que la SDL devra être incluse dans l'archive ou pouvoir s'installer avec les packages
* Les assets de votre application (dans le répertoire data, ex. images, sons, modèles 3D, fonts, fichiers de configuration)
* Les exécutables sont compilés dans le répertoire bin
* La documentation de votre projet (dans le répertoire doc) contenant au minimum
  * La présentation orale (fichier PowerPoint ou pdf)
  * Le diagramme des classes UML à jour (fichier image ou pdf)
  * Le diagramme de Gantt à jour (fichier image ou pdf) avec une description de qui a fait quoi dans le projet (associations entre les tâches du diagramme et les étudiants)
  * La documentation du code (dans le répertoire doc/html, générée par doxygen)

Veillez à bien nettoyer votre archive avant soumission, i.e. supprimer les fichiers inutiles (fichiers objets, dossiers/fichiers git etc.).  
