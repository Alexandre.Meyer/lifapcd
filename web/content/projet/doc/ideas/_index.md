# Voici une liste non exhaustive d'idées de sujet


## Jeux

Le plus rigolo c'est les jeux ... inspirez vous des mini-jeux de vos mobiles ou des consoles.

#### Plateaux
- Dames
- Les dames chinoises
- Othello
- Reversi
- Sudoku et variantes
- Démineur
- Risk, ou autre jeu de stratégie basé sur l'occupation du plateau
- Monopoly
- Scrabble
- etc.

#### Jeux de cartes
- Poker
- Munchkin
- Tarot
- etc.

#### Jeux d'arcade
- Pacman
- Tetris
- Nibble
- Sokoban
- Space invaders
- Azerty warrior
- Mini-course de voitures caméra au-dessus [type SuperSprint](http://en.wikipedia.org/wiki/Super_Sprint)
- Galaga
- Donkey Kong
- Star Wars
- Dig Dug
- Asteroids
- Defender
- Tron
- Tempest (tie)
- Centipede (tie)
- Snake
- etc.


## Simulation et vie artificielle

- génération de plantes à partir d'une grammaire (plutôt pour ceux qui ont déjà suivi "Infographie")
- génération automatique de terrain (plutôt pour ceux qui ont déjà suivi "Infographie") :
    cf. "Real Time Procedural Terrain Generation"\]\]
- vie artificielle (jeux de la vie, colonie de fourmies ? les sims ?)


## Applications
- Logiciel de traitement d'images : Application affichant une image et proposant différents algorithmes de traitement d'images : éclaircissement, niveau de gris, flou, etc.
- Gestion de rendez-vous / agenda
- Gestion de bibliothèques / dvd, etc.
- Musique : Gestion de playlist et autres
- Geographie : affichage du monde avec des infos sur les pays et villes
- Math : calcul formel, résolution de système, calcul matriciel, vecteur, etc.
- Logiciel de dessin vectoriel : On désire réaliser un logiciel de dessin vectoriel. Par vectoriel on entend qu'un dessin est constitué d'une liste de primitives géométriques simples : points, droites, cercles, carrés, ... auxquelles on peut par exemple associer des caractéristiques comme la couleur ou l'épaisseur du trait, etc. Pour être utilisable ce logiciel devra être muni d'une interface : par exemple une fenêtre constituée d'une barre d'outils, d'une zone de dessin et d'un menu. Vous êtes entièrement libre des outils que vous proposez à l'utilisateur ainsi que de la manière dont vous pensez votre interface. Prévoir un fichier d'aide pour l'utilisateur peut être une bonne idée si vous avez le temps.
