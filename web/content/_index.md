# Conception et développement d'applications

Responsables de l'enseignement : [Alexandre Meyer](http://liris.cnrs.fr/alexandre.meyer) et [Nicolas Pronost](http://liris.cnrs.fr/nicolas.pronost)

Volume horaire : 9h de CM, 12h de TD et 39h de TP


![lifapcd.png](images/lifapcd_logo.png)

## Objectif de l'UE

<p style="text-align:justify;">L'UE «Conception et Développement d'Applications » se trouve dans l'ensemble des UE d'algorithmique et de programmation de la licence STS mention Informatique en 2e année (L2). L'objectif de cette UE est de donner une première expérience de développement d'une application informatique de taille assez conséquente pour des étudiants ayant suivi déjà 3 UE d'algorithmique et de programmation avant. Une part importante de cette UE est consacrée à la conception et au développement d'une application durant les TP, par groupe de 2 ou 3 étudiants. Nous abordons essentiellement trois domaines de compétences.

Conception de l'architecture d'un logiciel : programmation modulaire, diagramme de dépendances des classes, boucle d'évènements, utilisation de librairies externes, multi-plateforme.\

Outils d'aide à la mise au point de programme : débogueur, documentation du code, analyse de performances, production de l'application à partir du code (make/cmake), gestion de version de code (git).\

Gestion de projet : cahier des charges, diagramme de Gantt, documentations, rapport, démo, présentation.
</p>


### Cours

La page des [cours](cours) regroupe les pdf des CM, les modcalités d'évaluation et des anciens sujets.


### TD

La page des [TD](td) regroupe les pdf des TD et toutes les informations sur le module Image à rendre.


### Projets

La page des [Projets](projet) regroupe toutes les informations sur le déroulement et les évaluations autour du projet.


### Doc
Le [dépôt git de l'UE](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp) regroupe 

* de nombreux exemples de code (SDL_simple, Pacman, SDL_ImGUI)
* [ainsi que des documentations pour les installations](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/tree/master/doc)
  * [Installer WSL](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/wsl.md) (WSL=Linux sous windows)
  * Choisir et installer un [éditeur de code](https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp/-/blob/master/doc/ide.md)
  * Installer les librairies (SDL2 notamment)
  * etc.

### Évaluation (les différentes notes)

* Un contrôle mi-parcours de 1h30 (40%) avant la phase de réalisation du projet
  * [Sujet Conception 2018](cours/doc/examens/2018_LIFAP4_CC_conception.pdf) (Jeux Olympique) /  [Sujet QCM 2018](cours/doc/examens/2018_LIFAP4_CC_QCM.pdf)
  * [Sujet Conception  2019](cours/doc/examens/2019_LIFAP4_CC_conception.pdf) (Pâtisserie), [un corrigé](cours/doc/examens/2020_corrige_exam_boulangerie.txt)  /  [Sujet QCM 2019](cours/doc/examens/2019_LIFAP4_CC_QCM.pdf)
  * [Sujet Conception  2020](cours/doc/examens/2020_LIFAP4_CC_conception.pdf)  (Duplo), voir le corrigé en TD /  [Sujet QCM 2020](cours/doc/examens/2020_LIFAP4_CC_QCM.pdf)
  * [Sujet Conception 2021](cours/doc/examens/2021_LIFAPCD_CC_conception.pdf) (Simulateur de galaxies)
  * [Sujet Conception 2022](cours/doc/examens/2022_LIFAPCD_CC_conception.pdf) (Jeu d'échec), [un corrigé](cours/doc/examens/2022_corrige_exam_echec.txt) 
  * [Sujet Conception 2023](cours/doc/examens/2023_LIFAPCD_CC_conception.pdf) (App/Montre de sport)
  * [Sujet Conception 2024](cours/doc/examens/2024_LIFAPCD_CC_conception.pdf) (App/Gestion de l'eau), [un corrigé](cours/doc/examens/2024_corrige_exam_eau.txt)

* Évaluation du module "Image" (TDs outils) (10%) 
* Projet (50%) :
  * Cahier des charges (2%)
  * Démo mi-*projet (3%), 
  * 3 notes finales "Technique", "Conception" et "Organisation" (3×15%). Regardez dans la [section Projet](projet) pour le détail des notes.

